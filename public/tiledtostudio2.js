const divWrapper = document.getElementById("wrapper");
const divBarraTopo = document.getElementById("barraTopo");
const divBarraRoomGMS2 = document.getElementById("barraRoomGMS2");
//const divBarraTilesetGMS2 = document.getElementById("barraTilesetGMS2");
const divBarraTiled = document.getElementById("barraTiled");
const divBarraMapaTiles = document.getElementById("barraMapaTiles");
const divBarraStatus = document.getElementById("barraStatus");
const inputArquivoRoomGMS2 = document.getElementById("arquivoRoomGMS2");
//const inputArquivoTilesetGMS2 = document.getElementById("arquivoTilesetGMS2");
const inputArquivoTiled = document.getElementById("arquivoTiled");
const buttonImportarDados = document.getElementById("importarDados");
const buttonBaixarRoom = document.getElementById("baixarRoom");

var jsonArquivoRoomGMS2=null;
var jsonArquivoTilesetGMS2=null;
var xmlArquivoTiled=null;
var layerRoomGMS2Selecionada=null;
var layerTiledSelecionada=null;
var importacaoTiledGMS2=false;
var dadosModificados=false;

function uploadArquivoRoomGMS2(e) {
    var arquivo=e.target.files[0];
    var leitor=new FileReader();
    leitor.readAsText(arquivo);
    leitor.onload=function(e) {
        var stringArquivo=e.target.result;
        stringArquivo=stringArquivo.replace(/\n/g,"");
        stringArquivo=stringArquivo.replace(/\t/g,"");
        stringArquivo=stringArquivo.replace(/,\s*}/g,"}");
        stringArquivo=stringArquivo.replace(/,\s*]/g,"]");
        //console.log(stringArquivo);
        jsonArquivoRoomGMS2=JSON.parse(stringArquivo);
        layerRoomGMS2Selecionada=null;
        importacaoTiledGMS2=false;
        console.log(jsonArquivoRoomGMS2);
        exibirRoomGMS2();
        statusAtual();
    }
}
inputArquivoRoomGMS2.onchange=uploadArquivoRoomGMS2;

/*
function uploadArquivoTilesetGMS2(e) {
    var arquivo=e.target.files[0];
    var leitor=new FileReader();
    leitor.readAsText(arquivo);
    leitor.onload=function(e) {
        var stringArquivo=e.target.result;
        stringArquivo=stringArquivo.replace(/\n/g,"");
        stringArquivo=stringArquivo.replace(/\t/g,"");
        stringArquivo=stringArquivo.replace(/,\s*}/g,"}");
        stringArquivo=stringArquivo.replace(/,\s*]/g,"]");
        //console.log(stringArquivo);
        jsonArquivoTilesetGMS2=JSON.parse(stringArquivo);
        importacaoTiledGMS2=false;
        console.log(jsonArquivoTilesetGMS2);
        exibirTilesetGMS2();
        statusAtual();
    }
}
inputArquivoTilesetGMS2.onchange=uploadArquivoTilesetGMS2;*/

function uploadArquivoTiled(e) {
    var arquivo=e.target.files[0];
	if (!arquivo.type=='text/xml') {
		console.log('Tipo de arquivo inválido = '+arquivo.type);
	}
    var leitor=new FileReader();
    leitor.readAsText(arquivo);
	leitor.onload=function(e) {
        var parser=new DOMParser();
        stringArquivo=e.target.result;
        stringArquivo=stringArquivo.replace(/\n/g,"");
        stringArquivo=stringArquivo.replace(/\t/g,"");
        stringArquivo=stringArquivo.replace(/\r/g,"");
        xmlArquivoTiled=parser.parseFromString(stringArquivo,'text/xml');
        layerTiledSelecionada=null;
        importacaoTiledGMS2=false;
        console.log(xmlArquivoTiled);
        exibirTiled();
        statusAtual();
    }
}
inputArquivoTiled.onchange=uploadArquivoTiled;

function exibirRoomGMS2() {
    divBarraRoomGMS2.innerHTML="";
    if (jsonArquivoRoomGMS2.resourceType=="GMRoom") {
        var numLayers=jsonArquivoRoomGMS2.layers.length;
        function criarBotoesRoom(argNome,argTipo,argNumero) {
            var n_divBotaoRoom=document.createElement("div");
            var n_pBotaoRoomTipo=document.createElement("p");
            n_pBotaoRoomTipo.innerHTML=argTipo;
            n_pBotaoRoomTipo.classList.add("tipoLayer");
            if (argTipo=="GMRTileLayer") {
                tileLayerEncontrada=true;
                n_divBotaoRoom.classList.add("tileLayer");
                n_divBotaoRoom.addEventListener("click",function(e){
                    console.log(e);
                    selecionarLayerRoomGMS2(argNumero,n_divBotaoRoom);
                },false);
            } else {
                n_divBotaoRoom.classList.add("outraLayer");
            }
            var n_pBotaoRoomNome=document.createElement("p");
            n_pBotaoRoomNome.innerHTML=argNome;
            n_divBotaoRoom.appendChild(n_pBotaoRoomTipo);
            n_divBotaoRoom.appendChild(n_pBotaoRoomNome);
            divBarraRoomGMS2.appendChild(n_divBotaoRoom);
        }
        var n_divDescricaoRoom=document.createElement("div");
        var n_pDescricaoRoom=document.createElement("p");
        n_pDescricaoRoom.innerHTML="Size: "+jsonArquivoRoomGMS2.roomSettings.Width+" x "+jsonArquivoRoomGMS2.roomSettings.Height+"<br>";
        n_pDescricaoRoom.innerHTML+=numLayers+" layers found<br>";
        n_divDescricaoRoom.appendChild(n_pDescricaoRoom);
        divBarraRoomGMS2.appendChild(n_divDescricaoRoom);
        var tileLayerEncontrada=false;
        for (var i=0; i<numLayers; i++) {
            criarBotoesRoom(jsonArquivoRoomGMS2.layers[i].name,jsonArquivoRoomGMS2.layers[i].resourceType,i);
        }
        if (!tileLayerEncontrada) {
            var n_divCriarTileLayer=document.createElement("div");
            n_divCriarTileLayer.classList.add("alerta");
            var n_pCriarTileLayer=document.createElement("p");
            //n_pCriarTileLayer.innerHTML="Nenhuma TileLayer encontrada.<br>É necessário ter uma para poder usar essa ferramenta.<br><br>Clique no botão abaixo para gerar uma TileLayer que será usada no arquivo posteriormente.<br><br>(É recomendado criar a TileLayer do jeito normal, direto pelo GMS2!)";
            n_pCriarTileLayer.innerHTML="No Tile Layers found.<br>It needs at least one to use this tool.<br><br>Go back to GMS2 and create one there, or select another file!";
            //var n_buttonCriar=document.createElement("button");
            //n_buttonCriar.innerHTML="Criar TileLayer";
            n_divCriarTileLayer.appendChild(n_pCriarTileLayer);
            //n_divCriarTileLayer.appendChild(n_buttonCriar);
            divBarraRoomGMS2.appendChild(n_divCriarTileLayer);
        }
    } else {
        var n_divPrecisaRoom=document.createElement("div");
        n_divPrecisaRoom.classList.add("alerta");
        var n_pPrecisaRoom=document.createElement("p");
        n_pPrecisaRoom.innerHTML="This file is not a GMS2 Room!!";
        n_divPrecisaRoom.appendChild(n_pPrecisaRoom);
        divBarraRoomGMS2.appendChild(n_divPrecisaRoom);
    }
}
function selecionarLayerRoomGMS2(argNumero,argElementoLayer) {
    layerRoomGMS2Selecionada=argNumero;
    var elementosLayers=divBarraRoomGMS2.getElementsByClassName("tileLayer");
    for (var i=0; i<elementosLayers.length; i++) {
        elementosLayers[i].classList.remove("tileSelecionada");
    }
    argElementoLayer.classList.add("tileSelecionada");
    importacaoTiledGMS2=false;
    statusAtual();
}

function exibirTilesetGMS2() {
    divBarraTilesetGMS2.innerHTML="";
    if (jsonArquivoTilesetGMS2.resourceType=="GMTileSet") {
        var n_pDescricaoTileset=document.createElement("p");
        n_pDescricaoTileset.innerHTML="Tileset externo:";
        divBarraTilesetGMS2.appendChild(n_pDescricaoTileset);
        var n_divDadosTileset=document.createElement("div");
        var n_pDadosTileset=document.createElement("p");
        n_pDadosTileset.innerHTML="Tamanho: "+jsonArquivoTilesetGMS2.tileWidth+" x "+jsonArquivoTilesetGMS2.tileHeight+"<br>";
        n_divDadosTileset.appendChild(n_pDadosTileset);
        divBarraTilesetGMS2.appendChild(n_divDadosTileset);
    } else {
        var n_divPrecisaTileset=document.createElement("div");
        n_divPrecisaTileset.classList.add("alerta");
        var n_pPrecisaTileset=document.createElement("p");
        n_pPrecisaTileset.innerHTML="O arquivo selecionado não é uma tileset do GMS2!!";
        n_divPrecisaTileset.appendChild(n_pPrecisaTileset);
        divBarraTilesetGMS2.appendChild(n_divPrecisaTileset);
    }
}
function informarTilesetInterno(argLayer) {
    divBarraTilesetGMS2.innerHTML="";
    var n_pDescricaoTileset=document.createElement("p");
    n_pDescricaoTileset.innerHTML="Tileset interno:";
    divBarraTilesetGMS2.appendChild(n_pDescricaoTileset);
    var n_divDadosTileset=document.createElement("div");
    var n_pNomeTileset=document.createElement("p");
    n_pNomeTileset.innerHTML="Nome: "+jsonArquivoRoomGMS2.layers[argLayer].tilesetId.name+"<br>";
    var n_pDadosTileset=document.createElement("p");
    n_pDadosTileset.innerHTML="Grade: "+jsonArquivoRoomGMS2.layers[argLayer].tiles.SerialiseWidth+" x "+jsonArquivoRoomGMS2.layers[argLayer].tiles.SerialiseHeight+"<br>";
    n_divDadosTileset.appendChild(n_pDadosTileset);
    divBarraTilesetGMS2.appendChild(n_divDadosTileset);
}

function exibirTiled() {
    divBarraTiled.innerHTML="";
    function criarBotoesTiled(argNome,argNumero) {
        var n_divBotaoTiled=document.createElement("div");
        //var n_pBotaoTiledTipo=document.createElement("p");
        //n_pBotaoTiledTipo.innerHTML=argTipo;
        //n_pBotaoTiledTipo.classList.add("tipoLayer");
        n_divBotaoTiled.classList.add("tileLayer");
        n_divBotaoTiled.addEventListener("click",function(e){
            console.log(e);
            selecionarLayerTiled(argNumero,n_divBotaoTiled);
        },false);
        var n_pBotaoTiledNome=document.createElement("p");
        n_pBotaoTiledNome.innerHTML=argNome;
        //n_divBotaoTiled.appendChild(n_pBotaoTiledTipo);
        n_divBotaoTiled.appendChild(n_pBotaoTiledNome);
        divBarraTiled.appendChild(n_divBotaoTiled);
    }
    var n_pDescricaoTiled=document.createElement("p");
    n_pDescricaoTiled.innerHTML="Tiled Map:";
    divBarraTiled.appendChild(n_pDescricaoTiled);
    var n_divDadosTiled=document.createElement("div");
    var n_pDadosTiled=document.createElement("p");
    var infoMapa=xmlArquivoTiled.getElementsByTagName("map")[0];
    console.log(infoMapa);
    n_pDadosTiled.innerHTML="Grid: "+infoMapa.getAttribute("width")+" x "+infoMapa.getAttribute("height")+"<br>";
    n_pDadosTiled.innerHTML+="Tiles: "+infoMapa.getAttribute("tilewidth")+" x "+infoMapa.getAttribute("tileheight")+"<br>";
    n_pDadosTiled.innerHTML+="Size: "+(parseInt(infoMapa.getAttribute("tilewidth"))*parseInt(infoMapa.getAttribute("width")))+" x "+(parseInt(infoMapa.getAttribute("tileheight"))*parseInt(infoMapa.getAttribute("height")))+"<br>";
    n_divDadosTiled.appendChild(n_pDadosTiled);
    divBarraTiled.appendChild(n_divDadosTiled);
    var infoLayers=infoMapa.getElementsByTagName("layer");
    for (var i=0; i<infoLayers.length; i++) {
        criarBotoesTiled(infoLayers[i].getAttribute("name"),i);
    }
}
function selecionarLayerTiled(argNumero,argElementoLayer) {
    layerTiledSelecionada=argNumero;
    var elementosLayers=divBarraTiled.getElementsByClassName("tileLayer");
    for (var i=0; i<elementosLayers.length; i++) {
        elementosLayers[i].classList.remove("tileSelecionada");
    }
    argElementoLayer.classList.add("tileSelecionada");
    var infoMapa=xmlArquivoTiled.getElementsByTagName("map")[0];
    var arrayMapa=infoMapa.getElementsByTagName("layer")[layerTiledSelecionada].getElementsByTagName("data")[0].innerHTML.split(",");
    var numeroTiles=arrayMapa.length;
    var n_divMapaTiles=document.createElement("div");
    n_divMapaTiles.classList.add("mapaTiles");
    n_divMapaTiles.style.gridTemplateColumns="repeat("+infoMapa.getAttribute("width")+", 20px)";
    n_divMapaTiles.style.gridTemplateRows="repeat("+infoMapa.getAttribute("height")+", 20px)";
    for (var i=0; i<numeroTiles; i++) {
        var n_divMapaTiles_tile=document.createElement("div");
        n_divMapaTiles_tile.innerHTML=arrayMapa[i];
        n_divMapaTiles.appendChild(n_divMapaTiles_tile);
    }
    divBarraMapaTiles.innerHTML="";
    divBarraMapaTiles.appendChild(n_divMapaTiles);
    importacaoTiledGMS2=false;
    statusAtual();
}

function inserirInfoTiledGMS2() {
    var infoMapa=xmlArquivoTiled.getElementsByTagName("map")[0];
    var arrayMapa=infoMapa.getElementsByTagName("layer")[layerTiledSelecionada].getElementsByTagName("data")[0].innerHTML.split(",");
    arrayMapa.forEach(function(argValor,argIndice){
        if (parseInt(argValor)-1<0) {
            arrayMapa[argIndice]=2147483648
        } else {
            arrayMapa[argIndice]=parseInt(argValor)-1;
        }
    });
    jsonArquivoRoomGMS2.roomSettings.Width=(parseInt(infoMapa.getAttribute("tilewidth"))*parseInt(infoMapa.getAttribute("width")));
    jsonArquivoRoomGMS2.roomSettings.Height=(parseInt(infoMapa.getAttribute("tileheight"))*parseInt(infoMapa.getAttribute("height")));
    jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseWidth=parseInt(infoMapa.getAttribute("width"));
    jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseHeight=parseInt(infoMapa.getAttribute("height"));
    jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.TileSerialiseData=arrayMapa;
    console.log(jsonArquivoRoomGMS2);
    importacaoTiledGMS2=true;
    statusAtual();
}

function downloadRoomGMS2() {
    var arquivoDownload=document.createElement('a');
	arquivoDownload.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(JSON.stringify(jsonArquivoRoomGMS2)));
	arquivoDownload.setAttribute('download', "room.yy");
	arquivoDownload.style.display = 'none';
	document.body.appendChild(arquivoDownload);
	arquivoDownload.click();
	document.body.removeChild(arquivoDownload);
}

function statusAtual() {
    divBarraStatus.innerHTML="";
    buttonImportarDados.disabled=true;
    buttonBaixarRoom.disabled=true;
    if (jsonArquivoRoomGMS2==null) {
        var n_pInfo=document.createElement("p");
        n_pInfo.innerHTML="Please select a .yy room file from GMS2.";
        n_pInfo.classList.add("info");
        divBarraStatus.appendChild(n_pInfo);
    } else if (layerRoomGMS2Selecionada==null) {
        var n_pInfo=document.createElement("p");
        n_pInfo.innerHTML="Please select a Tile Layer of the GMS2 Room selected.";
        n_pInfo.classList.add("info");
        divBarraStatus.appendChild(n_pInfo);
    } else if (xmlArquivoTiled==null) {
        var n_pInfo=document.createElement("p");
        n_pInfo.innerHTML="Please select a .tmx tile map file from Tiled Map Editor.";
        n_pInfo.classList.add("info");
        divBarraStatus.appendChild(n_pInfo);
    } else if (layerTiledSelecionada==null) {
        var n_pInfo=document.createElement("p");
        n_pInfo.innerHTML="Please select a Tile Layer of the Tiled map selected.";
        n_pInfo.classList.add("info");
        divBarraStatus.appendChild(n_pInfo);
    } else if (!importacaoTiledGMS2) {
        var n_pInfo=document.createElement("p");
        n_pInfo.innerHTML="All set! You can import Tiled data to GMS2 data by clicking the button below.";
        n_pInfo.classList.add("info");
        divBarraStatus.appendChild(n_pInfo);
        buttonImportarDados.disabled=false;
    } else if (importacaoTiledGMS2) {
        var n_pOk=document.createElement("p");
        n_pOk.innerHTML="Import complete! You can now download the GMS2 Room with Tiled data!";
        n_pOk.classList.add("ok");
        divBarraStatus.appendChild(n_pOk);
        dadosModificados=true;
    }
    if (layerRoomGMS2Selecionada!=null) {
        if (jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tilesetId==null) {
            var n_pErro=document.createElement("p");
            n_pErro.innerHTML="The GMS2 Room Tile Layer selected doesn't have a tileset defined. You must define one on this layer, directly in GMS2.";
            n_pErro.classList.add("erro");
            divBarraStatus.appendChild(n_pErro);
        }
    }
    if ((jsonArquivoRoomGMS2!=null) && (xmlArquivoTiled!=null)) {
        if (parseInt(jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseWidth)!==parseInt(xmlArquivoTiled.getElementsByTagName("map")[0].getAttribute("width"))) {
            var n_pAviso=document.createElement("p");
            n_pAviso.innerHTML="GMS2 Room Tile Layer grid's width ("+parseInt(jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseWidth)+") doesn't match with Tiled tile layer grid's width ("+parseInt(xmlArquivoTiled.getElementsByTagName("map")[0].getAttribute("width"))+"). If continue, TiledToStudio2 will update GMS2 Room width to match grids.";
            n_pAviso.classList.add("aviso");
            divBarraStatus.appendChild(n_pAviso);
        }
        if (parseInt(jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseHeight)!==parseInt(xmlArquivoTiled.getElementsByTagName("map")[0].getAttribute("height"))) {
            var n_pAviso=document.createElement("p");
            n_pAviso.innerHTML="GMS2 Room Tile Layer grid's height ("+parseInt(jsonArquivoRoomGMS2.layers[layerRoomGMS2Selecionada].tiles.SerialiseHeight)+") doesn't match with Tiled tile layer grid's height ("+parseInt(xmlArquivoTiled.getElementsByTagName("map")[0].getAttribute("height"))+"). If continue, TiledToStudio2 will update GMS2 Room height to match grids.";
            n_pAviso.classList.add("aviso");
            divBarraStatus.appendChild(n_pAviso);
        }
        if (xmlArquivoTiled.getElementsByTagName("tileset").length>1) {
            var n_pAviso=document.createElement("p");
            n_pAviso.innerHTML="It looks like this Tiled .tmx file is using more than one tileset in it. If this happens in one layer, GMS2 will not read the tiles on those tilesets, because GMS2 only uses one tileset per layer.";
            n_pAviso.classList.add("aviso");
            divBarraStatus.appendChild(n_pAviso);
        }
    }
    if (dadosModificados) {
        buttonBaixarRoom.disabled=false;
    }
}
statusAtual();