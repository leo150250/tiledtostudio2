# TiledToStudio2

A web-based application written by Leandro Gabriel to import Tiled Map Editor data into Game Maker Studio 2 Room Tile layers!

Also, this tool works offline! Download it and use it at your own!

## What is this?
TiledToStudio2 allows you to import a well-built tilemap made in Tiled Map Editor into a Game Maker Studio 2 Room Tile layer, saving you time on your project!

## Why did you do this?
I'm making a game in GMS2 that has a lot of levels that uses tilemaps. Since GMS2 autotile capabilities is functional but not useful with terrains and wang sets, I and my team decided to use Tiled Map Editor to make these levels.

On behalf, Tiled Map Editor can export only to Game Maker Studio 1, not to Game Maker Studio 2 because of how tilemaps works on it.

## "I'm pretty sure that there's something that already does that!"
I've searched on internet for a tool that could help me and, I found **GMTiled**... But it doesn't work for GMS2.3 and above.

Again, I've searched for another tool and, found **GMTiled2** which, although it works in GMS2.3, it only import maps at runtime and, to make that work, you must include the .tmx files as included files and make a bunch of code to get it working.

So, after giving up, I asked myself: _"Why not make a tool that does what I want?"_

After seeing that the .yy files are in JSON format and, .tmx files are in XML format... here we are!

This tool works as if it "injects" Tiled tilemap data into one layer of a GMS2 room, exactly what I needed, so I can continue working on the tilemap made in Tiled - that uses almost all of it's potential, like terrain brushes and wangsets - inside GMS2's IDE!

## This app really works?
It has some bugs here and there and, I've not checked if isometric maps work on it... And sometimes it throws some errors in console but, hey, it's basic functions works!

Please, consider starring the project and help me to keep this project alive, and add some more resources to it! I know it has some flaws but, I'm counting on you (and everyone that's passing by. pls...)

(And, please, for God's sake, DON'T OVERWRITE YOUR ROOMS! I'M NOT RESPONSIBLE IF EVERYTHING CRUMBLES DOWN AND YOU DOESN'T HAVE A BACKUP!)

## What I need to do to start using it?
This program has instructions embedded on it. Just read and you're good to go! It's very simple. Just upload a .yy room file from GMS2, a .tmx tilemap file from Tiled, select what layer you want from Tiled to "import" into a selected layer from GMS2 and voilá!